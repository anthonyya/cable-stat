import React, { Component } from 'react';
import { Text,Modal,View,Alert,TextInput,Platform} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actions } from 'react-native-navigation-redux-helpers';
import {
  Item, 
  Label, 
  Input, 
  Spinner, 
  Container, 
  Badge, 
  Title, 
  Content, 
  Button, 
  Icon,
  List, 
  ListItem, 
  Left, 
  Right, 
  Body, 
  Switch, 
  Radio, 
  Picker, 
  Separator,
  Form
} from 'native-base';
import Header from '../../Header';
import styles from './styles';
import api from '../../../api';
import BTN from '../../Button';
import * as BillcodeActions from '../../../actions/billcodes';
import { createSelector } from 'reselect';
import NewCat from './newCat';
import {
  getBilcodeCategories
} from '../../../selectors';
const {
  reset,
  popRoute,
  jumpTo
} = actions;

function randomString() {
  let length=20;
  let text = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for(let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}
function bindAction(dispatch) {
  return {
    ...bindActionCreators(BillcodeActions, dispatch),
    popRoute: key => dispatch(popRoute(key)),
    jumpTo: (i,tab) => dispatch(jumpTo(i,tab))
  };
}
const mapStateToProps = createSelector(
   getBilcodeCategories,
   (state)=>state.billcodes.loading,
   (state)=>state.cardNavigation,
   (categories,loading,navigation)=>{
    return {
      categories,
      loading,
      navigation
    }
   }
)

class NewCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      name:'',
      categoryId:'',
      pricePerOne:'',
      textInputValue:'',
      quantity:false
    }
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  handleComplete = () =>{
    if (this.state.name.length == 0) {
      Alert.alert(
          'Validation error',
          'Invalid name',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
      return;
    }
    if (this.state.pricePerOne.length == 0 || isNaN(+this.state.pricePerOne)) {
      Alert.alert(
          'Validation error',
          'Invalid Price Per One',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
      return;
    }
    if (!this.state.categoryId) {
      Alert.alert(
          'Validation error',
          'Invalid group',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
      return;
    }
    const newItem = {
      quantity:this.state.quantity,
      id:randomString(),
      name:this.state.name,
      pricePerOne: this.state.pricePerOne,
      categoryId: this.state.categoryId
    };
    this.props.addBillCode(newItem).then(resp=>{
      this.setModalVisible(false);
      this.props.popRoute(this.props.navigation.key);
      this.props.getBillCodes();
    })
    .catch((e)=>{
      console.log(e);
      console.log(e.message);
      Alert.alert(
          'Server error',
          'Invalid input data ('+e.message+')',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
    })
    
  }
  handleCancel = () =>{
    this.props.onCancel();
  }
  onCatAdded = (categoryId) =>{
    this.setState({
      categoryId
    });
    this.setModalVisible(false);
  }
  
	render(){
    const selectedCategory = _.find(this.props.categories,(c)=> {return c.id == this.state.categoryId} );
		return (
			<Container>
        <Header
          title="New Code"
          foreground="dark"
          leftItem={{
            title: 'Back',
            onPress: this.props.popRoute.bind(this,this.props.navigation.key),
          }}
        />
        <Content >
          <Separator bordered noTopBorder />
          <View style={styles.bgw}>
            <Form>
                  <Item stackedLabel last>
                      <Label>Name</Label>
                      <Input
                        placeholder="Name"
                        value={this.state.name}
                        onChangeText={(name) => this.setState({ name })}
                      />
                  </Item>
                  <Item stackedLabel last>
                      <Label>Price</Label>
                      <Input
                        placeholder="Price"
                        value={this.state.pricePerOne}
                        onChangeText={(pricePerOne) => this.setState({ pricePerOne })}
                      />
                  </Item>
              </Form>
              <Separator bordered noTopBorder />
              <ListItem icon >
                <Left>
                  <Button style={{ backgroundColor: '#FF9501' }}>
                    <Icon active name="md-checkmark-circle-outline" />
                  </Button>
                </Left>
                <Body>
                  <Text>Quantity changer</Text>
                </Body>
                <Right>
                    <Switch onValueChange={(quantity)=>this.setState({quantity})} value={this.state.quantity} />
                </Right>
            </ListItem>
              <ListItem icon onPress={this.setModalVisible.bind(this,true)} last>
                <Left>
                  <Button style={{ backgroundColor: '#007AFF' }}>
                    <Icon active name="md-create" />
                  </Button>
                </Left>
                <Body>
                  <Text>{selectedCategory ? "Selected category: "  : "Select category"}</Text>
                </Body>
                <Right >
                  <Text>{selectedCategory ? selectedCategory.name : 'None'}</Text>
                  {
                    (Platform.OS === 'ios')
                    &&
                    <Icon active name="arrow-forward" />
                  }
                </Right>
              </ListItem>
              <Separator bordered />
                <View style={styles.btnBlock}>
                  <BTN
                    style={{marginTop:10}}
                    type="primary"
                    caption="Save"
                    onPress={this.handleComplete}
                  />
                  <BTN
                    style={{marginTop:10}}
                    type="secondary"
                    caption="Cancel"
                    onPress={this.props.popRoute.bind(this,this.props.navigation.key)}
                  />
                </View>
              </View>

              <NewCat
                onDone={this.onCatAdded}
                onNewCat={this.props.addBillcodeCategory}
                modalVisible={this.state.modalVisible}
                categories={this.props.categories}
                onCancel={this.setModalVisible.bind(this,false)}
              />
          </Content >
        </Container>
		)
	}
}
export default connect(mapStateToProps, bindAction)(NewCode);
