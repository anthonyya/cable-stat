
const React = require('react-native');

const { StyleSheet } = React;

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FBFAFA',
  },
  row: {
    flex: 1,
    alignItems: 'center',
  },
  view:{
    flex: 1,
    flexDirection:'column'
    //alignItems: 'center',
    //marginTop:-20
  },
  scrollview:{
    flex: 1,
    flexDirection:'column',
    padding: 0,
    margin:0
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignItems: 'center',
  },
  mt: {
    marginTop: 18,
  },
  leftSwipeItem: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 20
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 20
  },
  headText:{
    color:'#fff',
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  }
});
