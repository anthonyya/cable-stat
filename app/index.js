import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import App from './App';

export default class Index extends Component {
	constructor() {
      super();
      this.state = {
        isLoading: false,
        store: configureStore(() => this.setState({ isLoading: false })),
      };
    }
  render() { // eslint-disable-line
    return (
      <Provider store={this.state.store} >
        <App />
      </Provider>
    );
  }
}
