
import React, { Component } from 'react';
import { connect } from 'react-redux';
import navigateTo from '../../actions/sideBarNav';
import CheckBox from '../CheckBox';
import { 
  Modal,
  TouchableHighlight,
  View,
  Text,
  Animated,
  StyleSheet,
  ScrollView,
  Alert
} from 'react-native';
import { getStats } from '../../actions/stat';
import { getBillCodes } from '../../actions/billcodes';
import Colors from '../../lib/Colors';
import Item from './Item';
import { Content, Button } from 'native-base';
import _ from 'lodash';
import Header from '../Header';
import api from '../../api';
import { createSelector } from 'reselect';
import {
  getBilcodes,
  getBilcodeCategories
} from '../../selectors';
import { actions } from 'react-native-navigation-redux-helpers';

const {
  popRoute,
} = actions;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  divider:{
    color: '#1B3B79',
    backgroundColor: '#EAEAEA',
    paddingLeft: 6,
    paddingRight: 6,
    paddingBottom: 4,
    paddingTop: 4,
  },
  scrollview: {
    padding: 0,
    paddingBottom: 20 + 49,
  },
  separator: {
    backgroundColor: '#EAEAEA',
  },
  applyButton: {
    position: 'absolute',
    left: 0,
    right: 0,

    padding: 8,
    backgroundColor: '#ffffff',
  },
});
function randomString(length=10) {
  let text = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for(let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}
class NewStat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sum:0,
      modalVisible: false,
      temps:[]
    }
  }
  static propTypes = {

  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  componentWillMount() {
    this.props.getBillCodes();
  }
  componentWillReceiveProps(nextProps) {
    if ((this.state.sum !== 0 || this.state.temps.length > 0) && nextProps.tabs.index == 1) {
      this.props.getBillCodes();
      this.setState({
        sum:0,
        temps:[]
      });
    }
  }
  handleComplete = () =>{
    let { temps } = this.state;
    const newStatItem = {
      id: randomString(),
      notice:'',
      items: {temps}
    };
    console.log(newStatItem);
    api.addStat(newStatItem).then(resp=>{
      this.setModalVisible(true);
    }).catch(e=>{
      console.log(e);
    })
  }
  handlePressCheckBox = (itm) =>{
    console.log(itm);
    let { temps } = this.state;
    const item = _.find(temps,i=>i.id == itm.id);
    const indexInTemp = temps.indexOf(item);
    this.isSelected(item) ? temps.splice(indexInTemp, 1) : temps.push(itm);
    let sum = 0;
    for (let i = 0; i < temps.length; i++) {
      sum += (+temps[i].pricePerOne * temps[i].selectedQuantity);
    }
    this.setState({
      sum,
      temps
    })
  }
  isSelected = (item={}) =>{
    return _.find(this.state.temps,i=>i.id == item.id);
  }
  onReset = (itm)=>{
console.log(itm);
    if (this.isSelected(itm)) {
      let { temps } = this.state;
      const item = _.find(temps,i=>i.id == itm.id);
      const indexInTemp = temps.indexOf(item);
      temps.splice(indexInTemp, 1);
      temps.push(itm);
      let sum = 0;
      for (let i = 0; i < temps.length; i++) {
        sum += (+temps[i].pricePerOne * temps[i].selectedQuantity);
      }
      this.setState({
        sum,
        temps
      })
    }
  }
  renderGroup = (group)=>{
    const groupedList = _.groupBy(this.props.billcodes,(i) => {
      return i.categoryId;
    })
    if (groupedList[group.id]) {
      return (
        <View key={group.id}>
          <Text style={styles.divider}>{group.name || "NONE"}</Text>
          {
            groupedList[group.id].map((itm,i)=>{
              const color =  Colors.colorForTopic(this.props.billcodes.length, i);
              return (
                <Item
                  reset={this.onReset}
                  key={group.id+'_'+itm.id}
                  item={itm}
                  color={color}
                  isChecked={this.isSelected(itm)}
                  onToggle={this.handlePressCheckBox}
                />
              )
            })
          }
        </View>
      )
    }
    
  }
  render() {
    let rightItem;
    if (this.state.sum > 0) {
      rightItem = {
        title: 'Add',
        onPress: this.handleComplete,
      }
    }
    return (
      <View style={styles.container}>
        <Header
          title="Add new stat"
          foreground="dark"
          rightItem={rightItem}
          leftItem={{
            title: 'Back',
            onPress: this.props.popRoute.bind(this,this.props.navigation.key),
          }}
        />
        
        <Content>
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.scrollview}>
          
          {
            this.props.cats.length > 0
            &&
            this.props.cats.map(this.renderGroup)
          }
          </ScrollView>
          <Modal
            animationType={"slide"}
            transparent={false}
            visible={this.state.modalVisible}
            >
           <View style={[styles.container,{padding:30}]}>
            <View>
                <Text style={{color:'#1B3B79',fontSize:30,marginTop:100,marginBottom:100,textAlign:'center'}}>{this.state.sum}$</Text>
                <Button block success rounded onPress={() => {
                  this.props.getStats();
                  this.setModalVisible(!this.state.modalVisible);
                  this.setState({
                    sum:0,
                    temps:[]
                  })
                  this.props.popRoute(this.props.navigation.key);
                }}>

                <Text style={{color:'#ffffff'}}>Ok</Text>
                </Button>
              </View>
            </View>
          </Modal>
        </Content>
        
      </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    getStats: () => dispatch(getStats()),
    getBillCodes: () => dispatch(getBillCodes()),
    popRoute: (key)=>dispatch(popRoute(key)),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}
const mapStateToProps = createSelector(
   getBilcodes,
   getBilcodeCategories,
   (state)=>state.tabs,
   (state)=>state.cardNavigation,
   (state)=>state.app.prevTab,
   (billcodes,cats,tabs,navigation,prevTab)=>{
    return {
      cats,
      billcodes,
      tabs,
      navigation,
      prevTab
    }
   }
)

export default connect(mapStateToProps, bindAction)(NewStat);
