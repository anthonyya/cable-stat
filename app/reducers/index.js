
import { combineReducers } from 'redux';

import cardNavigation from './cardNavigation';
import user from './user';
import bilcodeCategories from './bilcodeCategories';
import stat from './stat';
import tabs from './tabs';
import app from './app';
import billcodes from './billcodes';


export default combineReducers({
  user,
  stat,
  billcodes,
  cardNavigation,
  tabs,
  app,
  bilcodeCategories
});
