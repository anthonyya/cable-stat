
import React, { Component } from 'react';
import { Image,View,Alert } from 'react-native';
import { connect } from 'react-redux';
const BusyIndicator = require('react-native-busy-indicator');
const loaderHandler = require('react-native-busy-indicator/LoaderHandler');

import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Content, InputGroup, Header, Title,  Button, Item, Label, Input, Body, Left, Right, Icon, Form, Text } from 'native-base';
import { registerUser } from '../../actions/user';
import { getBillCodes } from '../../actions/billcodes';

import BTN from '../Button';
import styles from './styles';
const {
  replaceAt,
} = actions;

const icon = require('../../../images/app.png');
const background = require('../../../images/shadow.png');

class Reagister extends Component {

  static propTypes = {
    setUser: React.PropTypes.func,
    replaceAt: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  constructor(props) {
    super(props);
    this.state = {
      loading:false,
      email: '',
      password: '',
      confirmPassword:'',
      inviteCode:'',
      errors:[]
    };
  }

  setUser(email,password,confirmPassword,inviteCode) {
    return this.props.setUser(email,password,confirmPassword,inviteCode);
  }

  replaceRoute(route) {
    this.props.replaceAt('register', { key: route }, this.props.navigation.key);
  }

  submit = () =>{
    this.setState({ errors:[]})
    loaderHandler.showLoader("Loading");
    this.setUser(this.state.email,this.state.password,this.state.confirmPassword,this.state.inviteCode)
    .then(res=>{
      loaderHandler.hideLoader();
      if (res.user && res.info) {
        setTimeout(()=>{
          this.props.getBillCodes(true);
          this.replaceRoute('home');
        },500)
      } else if (res.errors){
        this.setState({
          errors:res.errors
        })
      }
    }).catch(()=>{
      loaderHandler.hideLoader();
    });
  }
  render() {
    return (
      <View style={styles.container}>

        <Content>
          <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
            <Image source={icon} style={{ height: 150, width: 150 }} />
          </View>
          <Form>
            <Item fixedLabel last>
              <Label>Email</Label>
              <Input placeholder="EMAIL" onChangeText={email => this.setState({ email })} />
            </Item>
            <Item fixedLabel last>
              <Label>Password</Label>
              <Input
                    placeholder="PASSWORD"
                    secureTextEntry
                    onChangeText={password => this.setState({ password })}
                  />
            </Item>
            <Item fixedLabel last>
              <Label>Confirm password</Label>
              <Input
                    placeholder="Confirm password"
                    secureTextEntry
                    onChangeText={confirmPassword => this.setState({ confirmPassword })}
                  />
            </Item>
            <Item fixedLabel last>
              <Label>Invite code</Label>
              <Input placeholder="Invite code" onChangeText={inviteCode => this.setState({ inviteCode })} />
            </Item>
          </Form>
          {
            this.state.errors.length > 0
            &&

            this.state.errors.map((e,i)=>{
              return (<Text style={{color:'red',padding:10}}>{e.msg}</Text>)
            })
          }
          <BTN
            style={{margin:10}}
            type="primary"
            caption="Sign up"
            onPress={this.submit}
          />
          <BTN
            style={{margin:10}}
            type="secondary"
            caption="Login"
            onPress={()=>{this.replaceRoute('login')}}
          />

        </Content>
        <BusyIndicator />
      </View>
    );
  }
}

function bindActions(dispatch) {
  return {
     getBillCodes: (hard) => dispatch(getBillCodes(hard)),
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    setUser: (email,password,confirmPassword,inviteCode) => dispatch(registerUser(email,password,confirmPassword,inviteCode)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindActions)(Reagister);
