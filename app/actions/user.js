
import type { Action } from './types';
import api from '../api';

import {ActionTypes} from '../constants';

export function setUser(email:string,password:string):Action {
  return {
    type: ActionTypes.SET_USER,
    promise: api.login(email,password)
  };
}
export function registerUser(email:string,password:string,confirmPassword:string,inviteCode:string):Action {
  return {
    type: ActionTypes.SET_USER,
    promise: api.register(email,password,confirmPassword,inviteCode)
  };
}
export function unsetUser():Action {
  return (dispatch, getState) => {
    dispatch({
      type: ActionTypes.UNSET_USER
    });
    dispatch({
      type: ActionTypes.CLEAR_STATS
    });
  };
}
