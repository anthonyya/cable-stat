
import {
	ActionTypes
} from '../constants';
import _ from 'lodash';
const DEFAULT_STATE = {
	list:[],
	error:null,
	editing:{},
	loading:false
};
export default function billcodes(state = DEFAULT_STATE, action) {
		switch (action.type) {
				case ActionTypes.INIT_APP:{
					return {
						...state,
						error:null,
						loading:true
					};
				}
				case ActionTypes.CLEAR_BILLCODES:{
					return DEFAULT_STATE
				}
				case ActionTypes.EDIT_BILLCODE_SUCCESS:{
					return {
						...state,
						editing:action.response || {}
					};
				}
				case ActionTypes.GET_BILLCODES_SUCCESS:
					return {
						...state,
						list:_.orderBy(_.uniqBy(action.response,'id'), ['order'], ['asc']),
						error:null,
						loading:false
					};
				case ActionTypes.GET_BILLCODES_REQUEST:{
					return {
						...state,
						loading:true
					};
				}
				default: {
						return state;
				}
		}
}
