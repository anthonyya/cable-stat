/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import Index from './app/index';
import {
  AppRegistry
} from 'react-native';

export default class CableStat extends Component {
  render() {
    return (
      <Index />
    );
  }
}

AppRegistry.registerComponent('CableStat', () => CableStat);
