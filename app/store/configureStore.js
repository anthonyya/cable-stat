import { createStore, applyMiddleware, compose } from 'redux';
import createLogger from 'redux-logger';
import rootReducer from '../reducers';
import { AsyncStorage } from 'react-native';
import thunk from 'redux-thunk';
import { persistStore } from 'redux-persist';
import promise from './promise';


const middleware = [thunk,promise].filter(Boolean);


function configureStore(initialState) {
	let store = createStore(rootReducer,compose(
		applyMiddleware(...middleware,createLogger())
	));
	persistStore(store, { storage: AsyncStorage }, initialState);
	return store;
}

export default configureStore;
