import SortableListView from 'react-native-sortable-listview';
import Swipeable from 'react-native-swipeable';
import React, { Component } from 'react';
import { TouchableOpacity,Text,Modal,View,Alert,TouchableHighlight,ListView } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import {
  Item, 
  Label, 
  Input, 
  Spinner, 
  Container, 
  Badge, 
  Title, 
  Content, 
  Button, 
  Icon,
  List, 
  ListItem, 
  Left, 
  Right, 
  Body, 
  Switch, 
  Radio, 
  Picker, 
  Separator,
  Footer, 
  FooterTab 
} from 'native-base';
import { getBillCodes,getBillCode } from '../../../actions/billcodes';
import Header from '../../Header';
import styles from './styles';
import api from '../../../api';
import moment from 'moment';
import BTN from '../../Button';
import {sortBilcodes,deleteBilcode,addBillcodeCategory} from '../../../actions/billcodes';
import { createSelector } from 'reselect';
import BillCodeItem from '../../ListItems/Delatable';
import ModalPicker from 'react-native-modal-picker';
import NewCat from './newCat';
import navigateTo from '../../../actions/sideBarNav';

import {
  getBilcodes,
  getBilcodeCategories
} from '../../../selectors';
const {
  reset,
  popRoute,
  jumpTo
} = actions;

function randomString() {
  let length=20;
  let text = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for(let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}
class BillCodes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      name:'',
      categoryId:'',
      pricePerOne:'',
      textInputValue:'',
      catModalVisible:false,
      editable:false
    };
  }
  static propTypes = {
    list: React.PropTypes.arrayOf(React.PropTypes.object),
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }
  navigateTo(route) {
    this.props.navigateTo(route, 'home');
  }
  onToggleEdit = () => {
    this.setState({
      editable: !this.state.editable
    })
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  setCatModalVisible = (visible) => {
    this.setState({catModalVisible: visible});
  }
  navigateTo(route) {
    this.props.navigateTo(route, 'home');
  }
  jumpTo = (i) =>{
    this.props.jumpTo(i,'ApplicationTabs')
  }
  handleComplete = () =>{
    if (this.state.name.length == 0) {
      Alert.alert(
          'Validation error',
          'Invalid name',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
      return;
    }
    if (this.state.pricePerOne.length == 0 || isNaN(+this.state.pricePerOne)) {
      Alert.alert(
          'Validation error',
          'Invalid Price Per One',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
      return;
    }
    if (this.state.group.length == 0) {
      Alert.alert(
          'Validation error',
          'Invalid group',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
      return;
    }
    const newItem = {
      id:randomString(),
      name:this.state.name,
      pricePerOne: this.state.pricePerOne,
      group: this.state.group
    };
    api.addMyCode(newItem).then(resp=>{
      this.setModalVisible(false);
      this.jumpTo(0)
      this.props.getBillCodes();
    })
    .catch((e)=>{
      Alert.alert(
          'Server error',
          'Invalid input data',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
    })
    
  }
  popRoute() {
    this.props.popRoute(this.props.navigation.key);
  }
  onDelete = (code) =>{
    this.props.deleteBilcode(code.id);
  }
  onEdit = (code) =>{
    this.props.getBillCode(code.id).then(()=>{
      this.navigateTo('editbillcode');
    })
  }
  renderEditableRow = (code) => {

    return (
      <TouchableHighlight style={styles.row} key={code.id}>
        <View style={styles.row}>
          <BillCodeItem
            onPressDelete={this.onDelete.bind(this,code)}
            first={code.name}
            second={code.pricePerOne}
            mini={code.category ? code.category.name : 'None'}
          />
        </View>
      </TouchableHighlight>
      );
  }
  renderSingleRow = (code) => {
    return (
      <TouchableHighlight style={styles.row} key={code.id} onPress={this.onEdit.bind(this,code)}>
        <View style={styles.row}>
          <BillCodeItem
            first={code.name}
            second={code.pricePerOne}
            mini={code.category ? code.category.name : 'None'}
          />
        </View>
      </TouchableHighlight>
      );
  }
  onCatAdded = (categoryId) =>{
    this.setState({
      categoryId
    });
    this.setCatModalVisible(false);
  }
  render() {
    const data = _.keyBy(this.props.list,'order');
    const order = Object.keys(data);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    const editable = this.state.editable;
    return (
      <View style={styles.container}>
        <Header
          title="Billcodes"
          foreground="dark"
          leftItem={{
            title: editable ? 'Cancel' :'Edit',
            onPress:this.onToggleEdit
          }}
          rightItem={{
            title: 'Add',
            onPress: ()=>this.navigateTo('newbillcode'),
          }}
        />
        <View style={styles.container} >
        {
          this.props.loading
          &&
          <Spinner />
        }
        {
          this.props.list.length <= 0
          ?
          <View >
            <Text style={{padding:20,textAlign:'center'}}>Your billcode list is empty</Text>
            <BTN
                  style={{marginTop:10,padding:10}}
                  type="primary"
                  caption="Get default bilcodes"
                  onPress={this.props.getBillCodes.bind(this,true)}
                />
          </View >
          :
          this.state.editable
          ?
          <SortableListView
            key={'asssssd'}
            style={{flex: 1}}
            data={data}
            order={order}
            onRowMoved={e => {
              console.log(e);
              console.log(JSON.stringify(order));
              order.splice(e.to, 0, order.splice(e.from, 1)[0]);
              this.props.sortBilcodes({e,order});
            }}
            renderRow={this.renderEditableRow}
          />
          :
          <SortableListView
            key={'asd'}
            style={{flex: 1}}
            data={data}
            order={order}
            onRowMoved={e => {
              console.log(e);
              console.log(JSON.stringify(order));
              order.splice(e.to, 0, order.splice(e.from, 1)[0]);
              this.props.sortBilcodes({e,order});
            }}
            renderRow={this.renderSingleRow }
          />
        }
        </View>

      </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    popRoute: key => dispatch(popRoute(key)),
    getBillCodes: (hard) => dispatch(getBillCodes(hard)),
    jumpTo: (i,tab) => dispatch(jumpTo(i,tab)),
    sortBilcodes: (e) => dispatch(sortBilcodes(e)),
    deleteBilcode: (code) => dispatch(deleteBilcode(code)),
    addBillcodeCategory: (cat) => dispatch(addBillcodeCategory(cat)),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
    getBillCode: (id)=>dispatch(getBillCode(id))
  };
}

const mapStateToProps = createSelector(
   getBilcodes,
   getBilcodeCategories,
   (state)=>state.billcodes.loading,
   (state)=>state.cardNavigation,
   (list,categories,loading,navigation)=>{
    return {
      categories,
      list,
      loading,
      navigation
    }
   }
)
export default connect(mapStateToProps, bindAction)(BillCodes);
