//import { CheckBox } from 'native-base';
import React from 'react'
import CheckBox from '../CheckBox';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} from 'react-native';

class TopicItem extends React.Component {
  props: {
    topic: string;
    color: string;
    isChecked: boolean;
    onToggle: (value: boolean) => void;
  };

  render() {
    const {topic, color, isChecked, onToggle} = this.props;
    const style = isChecked
      ? {backgroundColor: color}
      : {borderColor: color, borderWidth: 1};
    const accessibilityTraits = ['button'];
    if (isChecked) {
      accessibilityTraits.push('selected');
    }
    return (
      <TouchableOpacity
        accessibilityTraits={accessibilityTraits}
        activeOpacity={0.8}
        style={styles.container}
        onPress={onToggle}>
        <CheckBox
          text={'1'}
          onToggle={onToggle}
          isChecked={isChecked}
          color={color}
        />
        <Text style={styles.title}>
          {topic}
        </Text>
        {
          this.props.children
        }
      </TouchableOpacity>
    );
  }
}

const SIZE = 24;

const styles = StyleSheet.create({
  container: {
    paddingLeft:20,
    paddingRight:20,
    paddingVertical: 12,
    flexDirection: 'row',
    borderBottomColor: '#0322500A',
    borderBottomWidth:1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  checkbox: {
    width: SIZE,
    height: SIZE,
    borderRadius: SIZE / 2,
    marginRight: 10,
  },
  title: {
    fontSize: 17,
    color: '#1B3B79',
    flex: 1,
  },
});

module.exports = TopicItem;
