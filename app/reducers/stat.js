
import {
	ActionTypes
} from '../constants';
import _ from 'lodash';
const DEFAULT_STATE = {
	list:[],
	editing:{},
	error:null,
	loading:false
};
export default function stat(state = DEFAULT_STATE, action) {
		switch (action.type) {
				case ActionTypes.INIT_APP:{
					return {
						...state,
						error:null,
						loading:true
					};
				}
				case ActionTypes.CLEAR_STATS:{
					return DEFAULT_STATE
				}
				case ActionTypes.GET_STATS_SUCCESS:
					return {
						...state,
						list:_.uniqBy(action.response,'id'),
						error:null,
						loading:false
					};
				case ActionTypes.GET_STATS_SUCCESS:
					
					return {
						...state,
						list:_.uniqBy(action.response,'id'),
						error:null,
						loading:false
					};
				case ActionTypes.EDITING_STAT_SUCCESS:{
					return {
						...state,
						editing:action.response || {}
					};
				}
				case ActionTypes.GET_STATS_REQUEST:{
					return {
						...state,
						loading:true
					};
				}
				default: {
						return state;
				}
		}
}
