import React from 'react'
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} from 'react-native';

class CheckBox extends React.Component {
  props: {
    color: string;
    isChecked: boolean;
    onToggle: (value: boolean) => void;
  };

  render() {
    const {
      topic,
      color,
      isChecked,
      onToggle,
      text
    } = this.props;
    const style = isChecked
      ? {backgroundColor: color}
      : {borderColor: color, borderWidth: 1};
    const textStyle = isChecked
      ? {color: '#fff'}
      : {color: color};
    const accessibilityTraits = ['button'];
    if (isChecked) {
      accessibilityTraits.push('selected');
    }
    return (
      <TouchableOpacity
        accessibilityTraits={accessibilityTraits}
        activeOpacity={0.8}
        style={[styles.checkbox, style]}
        onPress={onToggle}>
        <Text style={[styles.title,textStyle]}>{text}</Text>
      </TouchableOpacity>
    );
  }
}

const SIZE = 24;

const styles = StyleSheet.create({
  checkbox: {
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    borderRadius: SIZE / 2,
    marginRight: 5,
  },
  title: {
    fontSize: 12,
  },
});

module.exports = CheckBox;
