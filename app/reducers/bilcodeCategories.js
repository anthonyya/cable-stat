
import {
	ActionTypes
} from '../constants';
import _ from 'lodash';
const DEFAULT_STATE = {
	list:{}
};
export default function billcodeCategories(state = DEFAULT_STATE, action) {
		switch (action.type) {
				case ActionTypes.INIT_APP:{
					return {
						...state,
					};
				}
				case ActionTypes.CLEAR_BILLCODE_CATEGORIES:{
					return DEFAULT_STATE
				}
				case ActionTypes.GET_BILLCODE_CATEGORIES_SUCCESS:
					return {
						...state,
						list:action.response,
					};
				default: {
					return state;
				}
		}
}
