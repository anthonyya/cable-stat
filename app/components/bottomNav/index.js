
import React, { Component } from 'react';
import { Image } from 'react-native';
import { connect } from 'react-redux';
import { Content,Container, Text, List, ListItem,Footer, FooterTab,Button ,Badge,Icon} from 'native-base';

import { actions } from 'react-native-navigation-redux-helpers';
import { unsetUser } from '../../actions/user';
import navigateTo from '../../actions/sideBarNav';

const {
  reset,
  pushRoute,
} = actions;

class BottomBar extends Component {

  static propTypes = {
    navigateTo: React.PropTypes.func,
  }

  navigateTo(route) {
    this.props.navigateTo(route, 'home');
  }
  logout = () =>{
    this.props.logout();
    this.props.reset(this.props.navigation.key);
  }
  render() {
    return (
      <Footer >
            <FooterTab>
                <Button active onPress={() => this.navigateTo('home')}>
                  <Icon name="home" />
                  <Text>Home</Text>
                </Button>
            </FooterTab>
            <FooterTab>
                <Button onPress={() => this.navigateTo('newstat')}>
                    <Icon name="add" />
                    <Text>New</Text>
                </Button>
            </FooterTab>
            <FooterTab>
                <Button onPress={() => this.navigateTo('codes')}>
                    <Icon name="ios-barcode-outline" />
                    <Text>Codes</Text>
                </Button>
            </FooterTab>
            <FooterTab>
                <Button onPress={() => this.navigateTo('settings')}>
                    <Icon name="settings" />
                    <Text>Settings</Text>
                </Button>
            </FooterTab>
            
        </Footer>
    );
  }
}

function bindAction(dispatch) {
  return {
    reset: key => dispatch(reset([{ key: 'login' }], key, 0)),
    logout: () => dispatch(unsetUser()),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(BottomBar);
