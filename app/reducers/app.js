
import {
	ActionTypes
} from '../constants';

const DEFAULT_STATE = {
	prevTab:{},
};
export default function app(state = DEFAULT_STATE, action) {
		switch (action.type) {
				case ActionTypes.INIT_APP:{
					return {
							...state,
							error:null
					};
				}
				case 'react-native-navigation-redux-helpers/JUMP_TO_INDEX':{
					return {
							prevTab:action.payload
					};
				}
				default: {
						return state;
				}
		}
}
