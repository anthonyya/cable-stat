
import React, { Component } from 'react';
import { Image,View,Alert } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Content, InputGroup, Header, Title,  Button, Item, Label, Input, Body, Left, Right, Icon, Form, Text } from 'native-base';
import { setUser } from '../../actions/user';
import { getBillCodes } from '../../actions/billcodes';
const loaderHandler = require('react-native-busy-indicator/LoaderHandler');
import styles from './styles';
import BTN from '../Button';
const BusyIndicator = require('react-native-busy-indicator');
const {
  replaceAt,
} = actions;
const icon = require('../../../images/app.png');

class Login extends Component {

  static propTypes = {
    setUser: React.PropTypes.func,
    replaceAt: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  setUser(email,password) {
    return this.props.setUser(email,password);
  }

  replaceRoute(route) {
    this.props.replaceAt('login', { key: route }, this.props.navigation.key);
  }
  submit = () =>{
    loaderHandler.showLoader("Loading");
    this
    .setUser(this.state.email,this.state.password)
    .then(res=>{
      loaderHandler.hideLoader();
      if (res.user && res.info) {
        setTimeout(()=>{
          this.props.getBillCodes(true);
          this.replaceRoute('home');
        },500)
        
      } else {
        Alert.alert(
          'Validation error',
          'Invalid email or password',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
      }
    }).catch(()=>{
      loaderHandler.hideLoader();
      Alert.alert(
          'Validation error',
          'Invalid email or password',
          [
            {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: true }
        )
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Content>
          <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
            <Image source={icon} style={{ height: 150, width: 150 }} />
          </View>
          <Form>
            <Item fixedLabel last>
              <Label>Username</Label>
              <Input placeholder="EMAIL" onChangeText={email => this.setState({ email })} />
            </Item>
            <Item fixedLabel last>
              <Label>Password</Label>
              <Input
                    placeholder="PASSWORD"
                    secureTextEntry
                    onChangeText={password => this.setState({ password })}
                  />
            </Item>
          </Form>
          <BTN
            style={{margin:10}}
            type="primary"
            caption="Login"
            onPress={this.submit}
          />
          <BTN
            style={{margin:10}}
            type="secondary"
            caption="Sign up"
            onPress={()=>{this.replaceRoute('register')}}
          />
          
        </Content>
        <BusyIndicator />
      </View>
    );
  }
}

function bindActions(dispatch) {
  return {
    getBillCodes: (hard) => dispatch(getBillCodes(hard)),
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    setUser: (email,password) => dispatch(setUser(email,password)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindActions)(Login);
