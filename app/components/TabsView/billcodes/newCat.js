import React, { Component } from 'react';
import { TouchableOpacity,Text,Modal,View,Alert,TextInput,PickerIOS,StyleSheet,SegmentedControlIOS  } from 'react-native';
import {Item, Label, Input, Spinner, Container,  Badge, Title, Content, Button, Icon,List, ListItem, Left, Right, Body, Switch, Radio, Picker, Separator,Footer, FooterTab } from 'native-base';
import Header from '../../Header';
const PickerItemIOS = PickerIOS.Item;
import BTN from '../../Button';

const styles = StyleSheet.create({
  item: {
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: "#ddd"
  },
  header: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
    backgroundColor: "#efefef"
  },
  content: {
    backgroundColor: "white",
    padding: 10,
    flex: 1,
    flexDirection: "row",
    alignItems: 'center'
  }
});
export default class NewCat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textInputValue:'',
      newCat:'',
      selectedIndex:0
    }
  }
  handleComplete = () =>{
    if (this.state.newCat) {
      console.log(this.props);
      this.props.onNewCat({name:this.state.newCat})
      .then(res=>{
        console.log(res);
        if(res && res.id) {
          this.setState({
          textInputValue:'',
          newCat:'',
          selectedIndex:0
        });
        this.props.onDone(res.id);
        }
        
      });
      
      
    } else {
      this.props.onDone(this.state.textInputValue);
    }
    
  }
  handleCancel = () =>{
    this.props.onCancel();
  }
	render(){
		return (
			<View style={styles.content}>
				<Modal
          animationType={"slide"}
          transparent={false}
          visible={this.props.modalVisible}
          >
          <View style={[styles.container,{padding:30}]}>
            <View>
              <Text style={{color:'#1B3B79',fontSize:20,marginTop:50,marginBottom:50,textAlign:'center'}}>Add new Bill-Code Group</Text>
              <SegmentedControlIOS
                values={['Select','Create']}
                selectedIndex={this.state.selectedIndex}
                onChange={(event) => {
                  this.setState({
                    selectedIndex: event.nativeEvent.selectedSegmentIndex,
                  });
                }}
                />
                {
                  this.state.selectedIndex == 1
                  &&
                  <Item >
                    <Label>New Group</Label>
                    <Input
                          placeholder="E.g. Home"
                          onChangeText={newCat => this.setState({ newCat })}
                        />
                  </Item>
                }
                {
                  this.state.selectedIndex == 0
                  &&
                  <PickerIOS
                    ref={'picker'}
                    style={{}}
                    selectedValue={this.state.textInputValue}
                    onValueChange={(option)=>{ 
                      console.log(option);
                      this.setState({textInputValue:option})
                    }}
                  >
                    {
                      this.props.categories.map((option, index) => (
                        <PickerItemIOS
                        key={option.id}
                        value={option.id}
                        label={option.name}
                      />))
                    }
                  </PickerIOS>
                }
              <BTN
                style={{marginTop:10}}
                type="primary"
                caption="Done"
                onPress={this.handleComplete}
              />
              <BTN
                style={{marginTop:10}}
                type="secondary"
                caption="Cancel"
                onPress={this.handleCancel}
              />
            </View>
          </View>
        </Modal>
			</View>
		)
	}
}
