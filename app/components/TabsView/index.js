'use strict';
const{ StyleSheet } = require('react-native');

import Colors from '../../lib/Colors';
import BillCodes from './billcodes';
import Stat from './home';
import Settings from './settings';
import React from 'React';
import TabBarIOS from 'TabBarIOS';
import TabBarItemIOS from 'TabBarItemIOS'
import { connect } from 'react-redux';
import { actions as navigationActions } from 'react-native-navigation-redux-helpers';
const Icon = require("react-native-vector-icons/Ionicons");
const { jumpTo } = navigationActions;


const styles = StyleSheet.create({
  tab: {
    backgroundColor: '#FBFAFA',
    flex: 1,
    alignItems: 'center',
    justifyContent:'center'
  }
});

//import type {Tab, Day} from '../reducers/navigation';

class TabsView extends React.Component {
  
  onTabSelect(i,tab) {
    if (this.props.tab !== tab) {
      this.props.onTabSelect(i,'ApplicationTabs');
    }
  }

  render() {
    return (
      <TabBarIOS tintColor={Colors.darkText}>
        <Icon.TabBarItem
          title="Home"
          iconName="ios-home"
          selected={this.props.tab === 0}
          onPress={this.onTabSelect.bind(this, 0)}  >

          <Stat />

        </Icon.TabBarItem>
        <Icon.TabBarItem
          title="Billcodes"
          iconName="ios-barcode-outline"
          selected={this.props.tab === 1}
          onPress={this.onTabSelect.bind(this, 1)} >

          <BillCodes />

        </Icon.TabBarItem>
        <Icon.TabBarItem
          title="Settings"
          iconName="ios-settings"
          selected={this.props.tab === 2}
          onPress={this.onTabSelect.bind(this, 2)} >

          <Settings />

        </Icon.TabBarItem>
      </TabBarIOS>
    );
  }

}

function select(store) {
  return {
    navigation: store.tabs,
    tab: store.tabs.index
  };
}

function actions(dispatch) {
  return {
    onTabSelect: (i,tab) => dispatch(jumpTo(i,tab)),
  };
}

module.exports = connect(select, actions)(TabsView);
