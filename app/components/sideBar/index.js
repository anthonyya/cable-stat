
import React, { Component } from 'react';
import { Image } from 'react-native';
import { connect } from 'react-redux';
import { Content,Container, Text, List, ListItem} from 'native-base';

import { closeDrawer } from '../../actions/drawer';
import { actions } from 'react-native-navigation-redux-helpers';
import { setIndex } from '../../actions/list';
import { unsetUser } from '../../actions/user';
import navigateTo from '../../actions/sideBarNav';
import myTheme from '../../themes/base-theme';

import styles from './style';
const {
  reset,
  pushRoute,
} = actions;

class SideBar extends Component {

  static propTypes = {
    // setIndex: React.PropTypes.func,
    navigateTo: React.PropTypes.func,
  }

  navigateTo(route) {
    this.props.navigateTo(route, 'home');
  }
  logout = () =>{
    this.props.close();
    this.props.logout();
    this.props.reset(this.props.navigation.key);
  }
  render() {
    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: '#3e3e3e',top: -1 }}
        >
          <List>
            <ListItem button onPress={() => this.navigateTo('newstat')} >
              <Text style={{color:'#ffffff'}}>New stat</Text>
            </ListItem>
            <ListItem button onPress={() => this.navigateTo('settings')} >
              <Text style={{color:'#ffffff'}}>Settings</Text>
            </ListItem>
            <ListItem button onPress={this.logout} >
              <Text style={{color:'#ffffff'}}>Logout</Text>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    setIndex: index => dispatch(setIndex(index)),
    reset: key => dispatch(reset([{ key: 'login' }], key, 0)),
    logout: () => dispatch(unsetUser()),
    close: () => dispatch(closeDrawer()),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(SideBar);
