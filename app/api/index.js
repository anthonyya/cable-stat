import { AsyncStorage } from 'react-native';
import _ from 'lodash';
import axios from 'axios';
function randomString(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for(var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

function toJson(res) {
	return res ? JSON.parse(res) : [];
}
const API_URL = "https://kablebill.com/api/v1/";//52.34.105.222
export default {
	headers: new Headers(),
	init(){
		this.setHeaders();
	},

	setAuth(token){
		this.token = token;
		this.headers.append("Authorization", "Bearer "+token);
	},
	setHeaders(){
		this.headers.append("Content-Type", "application/json");
	},
	getBillcodeCategories(){
		return fetch(`${API_URL}categories`,{
			method: "GET",
			headers: this.headers
		})
		.then(categories => categories.json())
	},
	updateBillcodeCategory(id,cat){
		return fetch(`${API_URL}categories/${id}`,{
			method: "PUT",
			headers: this.headers,
			body: JSON.stringify(cat)
		})
		.then(response => {
			console.log(response);
			return response.json()
		})
	},
	addBillcodeCategory(cat){
		return fetch(`${API_URL}categories`,{
			method: "POST",
			headers: this.headers,
			body: JSON.stringify(cat)
		})
		.then(response => {
			console.log(response);
			return response.json()
		})
	},
	bulkUpdateBillCodes(list){
		return fetch(`${API_URL}billcodes/bulk`,{
			method: "PUT",
			headers: this.headers,
			body: JSON.stringify({list})
		})
		.then(response => {
			return response.json()
		})
	},
	getBillCodes(){
		return fetch(`${API_URL}billcodes`,{
			method: "GET",
			headers: this.headers
		})
		.then(response => response.json())
		.then((co)=>{
			return this.getBillcodeCategories()
			.then((categories)=>{
				console.log('test');
				return {codes:co,categories};
			})
			
		})
		.then(({codes,categories})=>{
			AsyncStorage.setItem('@RemoteBillCodes:all',JSON.stringify(codes));
			AsyncStorage.setItem('@RemoteBillCodesCategories:all',JSON.stringify(categories));
			return {codes,categories};
		})
		/*return Promise.all([
			AsyncStorage.getItem('@RemoteBillCodes:all').then(toJson),
			AsyncStorage.getItem('@RemoteBillCodesCategories:all').then(toJson)
		])
		.then(([codes,categories])=>{
			console.log('test',codes,categories);
			if (hard || (codes && codes.lengtn <= 0 && (categories && categories.lengtn <= 0))) {
				
			} else {
				console.log('codes,categories',codes,categories);
				return {codes,categories};
			}
		})*/
		
	},
	addMyCode(code){
		return fetch(`${API_URL}billcodes`,{
			method: "POST",
			headers: this.headers,
			body: JSON.stringify(code)
		})
		.then(response => {
			console.log(response);
			return response.json()
		})
	},
	getBillCode(id){
		return fetch(`${API_URL}billcodes/${id}`,{
			method: "GET",
			headers: this.headers
		})
		.then(response => {
			console.log(response);
			return response.json()
		})
	},
	deleteBillcode(id){
		return fetch(`${API_URL}billcodes/${id}`,{
			method: "DELETE",
			headers: this.headers
		})
		.then(response => {
			console.log(response);
			return response.json()
		})
	},
	updateBillCode(id,code){
		return fetch(`${API_URL}billcodes/${id}`,{
			method: "PUT",
			headers: this.headers,
			body: JSON.stringify(code)
		})
		.then(response => {
			console.log(response);
			return response.json()
		})
	},
	login(email,password){
		const path = `${API_URL}login`;
		return fetch(path,{
			method: "POST",
			headers: this.headers,
			body: JSON.stringify({email,password})
		})
		.then(response => response.json())
	},
	register(email,password,confirmPassword,inviteCode){
		const path = `${API_URL}signup`;
		return fetch(path,{
			method: "POST",
			headers: this.headers,
			body: JSON.stringify({email,password,confirmPassword,inviteCode})
		})
		.then(response => response.json())
	},
	me(){
		return fetch(`${API_URL}me`,{
				method: 'GET',
            	headers: this.headers,
            	mode: 'cors',
            	cache: 'default' })
		.then(response => response.json())
	},
	allStat() {
		return fetch(`${API_URL}cables`,{
				method: 'GET',
            	headers: this.headers,
            	mode: 'cors',
            	cache: 'default' })
		.then(response => response.json())
		//return AsyncStorage.getItem('@Stat:all').then(toJson);
	},
	oneStat(id) {
		return fetch(`${API_URL}cables/${id}/view`,{
				method: 'GET',
            	headers: this.headers,
            	mode: 'cors',
            	cache: 'default' })
		.then(response => response.json())
		//return AsyncStorage.getItem('@Stat:all').then(toJson);
	},
	updateStat(id,data) {
		return fetch(`${API_URL}cables/${id}`,{
				method: "PUT",
            	headers: this.headers,
            	body: JSON.stringify(data)
           })
		.then(response => response.json())
		//return AsyncStorage.getItem('@Stat:all').then(toJson);
	},
	deleteStat(id,data) {
		return fetch(`${API_URL}cables/${id}`,{
				method: "DELETE",
            	headers: this.headers
           })
		.then(response => response.json())
		//return AsyncStorage.getItem('@Stat:all').then(toJson);
	},
	addStat(stat){
		return fetch(`${API_URL}cables`,{
			method: "POST",
			headers: this.headers,
			body: JSON.stringify(stat)
		})
		.then(response => {
			console.log(response);
			return response.json()
		})
		/*.then(response=>{
			return AsyncStorage.getItem('@Stat:all')
			.then(toJson)
			.then(resp=>{
				resp.push(response);
				return AsyncStorage.setItem('@Stat:all',JSON.stringify(resp));
			})
			.then(toJson)
		})*/
		/*
		return AsyncStorage.getItem('@Stat:all')
		.then(toJson)
		.then(resp=>{
			resp.push(stat);
			return AsyncStorage.setItem('@Stat:all',JSON.stringify(resp));
		})
		.then(toJson);*/
		
	},
	removeStat(id){
		return AsyncStorage.getItem('@Stat:all')
		.then(toJson)
		.then(resp=>{
			for (var i = 0; i < resp.length; i++) {
				if (resp[i].id === id) {
					resp.splice(i,1);
				}
			}
			return AsyncStorage.setItem('@Stat:all',JSON.stringify(resp));
		})
		.then(toJson);
	},
	flushStat(){
		return AsyncStorage.removeItem('@Stat:all')
		.then(toJson)
	}
}