import {ActionTypes} from '../constants';
import type { Action } from './types';
import api from '../api';
export function getStats():Action {
  return {
    type: ActionTypes.GET_STATS,
    promise: api.allStat()
  };
}
export function getStat(id):Action {
  return {
    type: ActionTypes.EDITING_STAT,
    promise: api.oneStat(id)
  };
}
export function saveStat(id,data):Action {
  return {
    type: ActionTypes.EDITING_STAT_SAVE,
    promise: api.updateStat(id,data)
  };
}
export function deleteStat(id):Action {
  return {
    type: 'DELETE_STAT',
    promise: api.deleteStat(id)
  };
}
