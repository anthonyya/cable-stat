import {ActionTypes} from '../constants';
import type { Action } from './types';
import { AsyncStorage } from 'react-native';
import api from '../api';
function randomString(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for(var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}
function toJson(res) {
	return res ? JSON.parse(res) : [];
}
export function addBillCode(code):Action {
	return (dispatch, getState) => {
		const state = getState();
		code.order = state.billcodes.list.length;
		return dispatch({
		  type: ActionTypes.ADD_BILLCODE,
		  promise: api.addMyCode(code)
		})
	}
}
export function getBillCode(id):Action {
	return (dispatch, getState) => {
		return dispatch({
		  type: ActionTypes.EDIT_BILLCODE,
		  promise: api.getBillCode(id)
		})
	}
}

export function updateBillCode(code):Action {
	return (dispatch, getState) => {
		return dispatch({
		  type: ActionTypes.UPDATE_BILLCODE,
		  promise: api.updateBillCode(code.id,code)
		})
	}
}
export function getBillCodes():Action {
	return (dispatch, getState) => {
		dispatch({
			type: ActionTypes.GET_BILLCODES,
			promise: api.getBillCodes().then(avaliableList=>{
				const list = {};
				if (avaliableList.categories) {
					for (var i = 0; i < avaliableList.categories.length; i++) {
						list[avaliableList.categories[i].id] = avaliableList.categories[i];
					}
					dispatch({type:ActionTypes.GET_BILLCODE_CATEGORIES_SUCCESS,response:list});
				}
				return avaliableList.codes;
			})
		})
	}
}
export function sortBilcodes({e,order}):Action {
	return (dispatch, getState) => {
		api.getBillCodes().then(response=>{
			return response.codes;
		})
		//AsyncStorage.getItem('@RemoteBillCodes:all')
		//.then(toJson)
		.then(codes=>{
			const list = [];
			const codesKeys =  _.keyBy(codes,'order');
			for (var i = 0; i < order.length; i++) {
				codesKeys[order[i]].order = i;
				list.push(codesKeys[order[i]]);
			}
			dispatch({type:ActionTypes.GET_BILLCODES_SUCCESS,response:list})
			api.bulkUpdateBillCodes(list);
			return list;
		})
		.then(codes=>{
			
			return AsyncStorage.setItem('@RemoteBillCodes:all',JSON.stringify(codes))
		})
		.catch(e=>{
			console.log(e);
		})

	}
}
export function deleteBilcode(id):Action {
	return (dispatch, getState) => {
		return api.deleteBillcode(id).then(res=>{
			return api.getBillCodes().then(response=>{
				AsyncStorage.setItem('@RemoteBillCodes:all',JSON.stringify(response.codes))
				dispatch({type:ActionTypes.GET_BILLCODES_SUCCESS,response:response.codes})
				return response.codes
			})
		})
	}
}
export function addBillcodeCategory(cat):Action {
	return (dispatch, getState) => {
		return api.addBillcodeCategory(cat).then(category=>{
			return api.getBillcodeCategories().then(response=>{
				AsyncStorage.setItem('@RemoteBillCodesCategories:all',JSON.stringify(response))
				dispatch({type:ActionTypes.GET_BILLCODE_CATEGORIES_SUCCESS,response});
				return category
			})
		})
	}
}

