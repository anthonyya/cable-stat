
import React, { Component } from 'react';
import { Image,AsyncStorage,View,NetInfo} from 'react-native';
import { connect } from 'react-redux';
import { getBillCodes } from '../../actions/billcodes';
import { actions } from 'react-native-navigation-redux-helpers';
import api from '../../api';
const launchscreen = require('../../../images/app.png');
const {
  replaceAt,
} = actions;
class SplashPage extends Component {

  static propTypes = {
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }
  replaceRoute(route) {
    this.props.replaceAt('splashscreen', { key: route }, this.props.navigation.key);
  }
  componentWillMount() {
    api.init();
    AsyncStorage.getItem('@CurrentUser:me', (err, result) => {
      console.log(result);
      if (!result || err) {

        this.replaceRoute('login');
      } else {
        api.setAuth(JSON.parse(result).id);
        this.props.getBillCodes();
        this.replaceRoute('home');
      }
    });
  }

  render() {
    return (
      <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
        <Image source={launchscreen} style={{ height: 200, width: 200 }} />
      </View>
    );
  }
}


function bindActions(dispatch) {
  return {
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    getBillCodes: (hard) => dispatch(getBillCodes(hard)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});
export default connect(mapStateToProps, bindActions)(SplashPage);