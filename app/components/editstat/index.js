
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {View,Text,TextInput,ScrollView,} from 'react-native';
import { actions } from 'react-native-navigation-redux-helpers';
import Spinner from 'react-native-loading-spinner-overlay';
import { Container,Left, Right, Title, Content, Button, Icon,Body,Item, Form,Separator,
  Label, 
  Input,  } from 'native-base';
import BTN from '../Button';
import * as StatActions from '../../actions/stat';

import styles from './style';
import Header from '../Header';
import Colors from '../../lib/Colors';
import { bindActionCreators } from 'redux';
import { createSelector } from 'reselect';
import {
  getBilcodes,
  getBilcodeCategories
} from '../../selectors';
import Itm from '../newStat/Item';
const {
  reset,
  popRoute,
} = actions;

class EditStatPage extends Component {
  constructor(props) {
    super(props);
    this.state = {...props.stat}
  }
  popRoute = () => {
    this.props.popRoute(this.props.navigation.key);
  }
  logout = () =>{
    
  }
  componentWillReceiveProps(nextProps) {
    this.setState(nextProps.stat);
  }
  isSelected = (item) =>{
    return _.find(this.state.items.temps,i=>i.id == item.id)
  }
  getFromList = (id) =>{
    return _.find(this.state.items.temps,(i)=>i.id == id)
  }
  onSave = () =>{
    this.setState({
      loading:true
    })
    this.props.saveStat(this.state.id,this.state).then(()=>{
      this.setState({
        loading:false
      })
      this.props.getStats()
      this.props.popRoute(this.props.navigation.key)
    }).catch(()=>{
      this.setState({
        loading:false
      })
      this.props.popRoute(this.props.navigation.key)
    })
  }
  handlePressCheckBox = (item) =>{
    let temps = this.state.items.temps;
    const indexInTemp = temps.indexOf(this.getFromList(item.id));
    this.isSelected(item) ? temps.splice(indexInTemp, 1) : temps.push(item);
    this.setState({
      ...this.state,
      items:{
        temps
      }
    })
  }
  onReset = (itm)=>{
    if (this.isSelected(itm)) {
      const temps = this.state.items.temps;
      const item = _.find(temps,i=>i.id == itm.id);
      const indexInTemp = temps.indexOf(item);
      temps.splice(indexInTemp, 1);
      temps.push(itm);
      let sum = 0;
      for (let i = 0; i < temps.length; i++) {
        sum += (+temps[i].pricePerOne * temps[i].selectedQuantity);
      }
      this.setState({
        sum,
        temps
      })
    }
  }
  render() {
    return (
      <Container>
        <Header
          title="Edit"
          foreground="dark"
          leftItem={{
            title: 'Back',
            onPress: this.props.popRoute.bind(this,this.props.navigation.key),
          }}
          rightItem={{
            title: 'Save',
            onPress: this.onSave,
          }}
        />
        <Content >
          <View>
          
          <Form>
              <Item stackedLabel last>
                  <Label>Notice</Label>
                  <Input
                    placeholder="Enter Notice here"
                    value={this.state.notice}
                    onChangeText={(notice) => this.setState({ notice })}
                  />
              </Item>
          </Form>
          <Separator bordered noTopBorder />
          </View>
          <View style={styles.bgw}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.scrollview}>
            {
              this.props.billcodes.length > 0
              &&
              this.props.billcodes.map((itm,i)=>{
                console.log(itm);
                const color =  Colors.colorForTopic(this.props.billcodes.length, i);
                if (this.isSelected(itm)) {
                  itm.selectedQuantity = this.isSelected(itm).selectedQuantity
                }
                return(
                  <Itm
                    reset={this.onReset}
                    key={'_'+itm.id}
                    item={itm}
                    color={color}
                    isChecked={this.isSelected(itm)}
                    onToggle={this.handlePressCheckBox}
                  />
                  )
              })
            }
            </ScrollView>
            </View>
            <Spinner visible={this.state.loading} />
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    ...bindActionCreators(StatActions,dispatch),
    popRoute: key => dispatch(popRoute(key))
  };
}
const mapStateToProps = createSelector(
   getBilcodes,
   (state)=>state.cardNavigation,
   (state)=>state.stat.editing,
   (billcodes,navigation,stat)=>{
    return {
      billcodes,navigation,stat
    }
   }
);
export default connect(mapStateToProps, bindAction)(EditStatPage);
