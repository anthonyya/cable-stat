
const React = require('react-native');

const { StyleSheet } = React;

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
    backgroundColor: '#ffffff',
  },
  scrollview: {
    padding: 0,
    //paddingBottom: 20 + 49,
  },
  bgw:{
  	backgroundColor: '#FFF'
  }
});

