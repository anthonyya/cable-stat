
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Text} from 'react-native';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container,Left, Right, Title, Content, Button, Icon,Body } from 'native-base';
import { unsetUser } from '../../../actions/user';
import BTN from '../../Button';
import Header from '../../Header';
const {
  reset,
  popRoute,
} = actions;

class SettingsPage extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  popRoute = () => {
    this.props.popRoute(this.props.navigation.key);
  }
  logout = () =>{
    this.props.logout();
    this.props.reset(this.props.navigation.key);
  }
  /*<Button transparent onPress={() => this.popRoute()}>
                <Icon name="ios-arrow-back" />
              </Button>*/
  render() {
    return (
      <Container >
        <Header
          title="Settings"
          foreground="dark"
        />
        <Content padder>
          <BTN
                style={{marginTop:10}}
                type="primary"
                caption="Logout"
                onPress={this.logout}
              />
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    popRoute: key => dispatch(popRoute(key)),
    logout: () => dispatch(unsetUser()),
    reset: key => dispatch(reset([{ key: 'login' }], key, 0)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation
});


export default connect(mapStateToProps, bindAction)(SettingsPage);
