
import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text,
  Modal,
  ScrollView,
  RefreshControl,
  View,
  StatusBarIOS,
} from 'react-native';
import Swipeable from 'react-native-swipeable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actions as navigationActions } from 'react-native-navigation-redux-helpers';
import {
  Spinner,
  Container,
  //Header,
  Badge,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Left,
  Right,
  Body,
  Switch,
  Radio,
  Picker,
  Separator,
  Footer,
  FooterTab
} from 'native-base';
import Item from '../../ListItems/Delatable';

import Header from '../../Header';
import { Grid, Row } from 'react-native-easy-grid';
import navigateTo from '../../../actions/sideBarNav';
import * as StatActions from '../../../actions/stat';
import styles from './styles';
import api from '../../../api';
import moment from 'moment';
const {
  jumpTo
} = navigationActions;
function getSum(list=[]) {
  let sum = 0;
  for (var i = 0; i < list.length; i++) {
    sum += (+list[i].pricePerOne * (+list[i].selectedQuantity || 1));
  }
  return sum;
}
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      isRefreshing: false,
      editing:false
    }
  }
  static propTypes = {
    list: React.PropTypes.arrayOf(React.PropTypes.object),
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }
  componentWillMount() {
    this.props.getStats();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.list.length !== this.props.list.length) {
      this.props.getStats();
    }
    
  }
  setModalVisible = (visible) => {
    this.setState({modalVisible: visible});
  }
  toggleEditing = () =>{
    this.setState({
      editing:!this.state.editing
    })
  }
  navigateTo(route) {
    this.props.navigateTo(route, 'home');
  }
  jumpTo = (i) =>{
    this.props.jumpTo(i,'ApplicationTabs')
  }
  onEdit = (itm) =>{
    this.props.getStat(itm.id).then(res=>{
      this.navigateTo('editstat');
    });

  }
  onDelete = (itm) =>{
    this.props.deleteStat(itm.id).then(()=>{
      this.props.getStats();
    })
  }
  renderItemsByMonth = (key) => {
    const groupedList = _.groupBy(this.props.list,(i) => {
      return moment(i.createdAt).format("MMM dddd Do");
    })
    let sum = 0;
    for (var i = 0; i < groupedList[key].length; i++) {
      sum += +getSum(groupedList[key][i].items.temps);
    }
    const list = groupedList[key].map((item,index)=>{
      return (
        <TouchableOpacity key={item.id} onPress={this.onEdit.bind(this,item)}>
          <View >
            <Item
               onPressDelete={this.state.editing ? this.onDelete.bind(this,item) : null}
               key={item.id}
               first={getSum(item.items.temps)}
               second={'$'}
               mini={moment(item.createdAt).format("dddd") +' '+moment(item.createdAt).format("Do")}
               hideMove={true}
            />
          </View>
        </TouchableOpacity>
        
      )
    })
    list.unshift(<ListItem itemDivider>
      <Left>
        <Badge info>
            <Text style={styles.headText}>{sum}$</Text>
        </Badge>
      </Left>

      <Right>
        <Text>{key} 2017</Text>
      </Right>
    </ListItem>);
    return list;
  }
  _onRefresh = () =>{
    this.setState({isRefreshing: true});
    this.props.getStats().then(()=>{
      this.setState({isRefreshing: false});
    });
  }
  render() {
    const groupedList = _.groupBy(this.props.list,(i) => {
      return moment(i.createdAt).format("MMM dddd Do");
    })
    const groupedListKeys = Object.keys(groupedList);
    const rightItem = {
        title: 'Add',
        onPress: ()=>{this.navigateTo('new')},
      };
    return (
     <Container>
        <Header
          title="Home"
          foreground="dark"
          rightItem={rightItem}
          leftItem={{
            title: this.state.editing ? 'Cancel' :'Edit',
            onPress:this.toggleEditing
          }}
        />
        
        <View style={styles.view}>
          <ScrollView
            pagingEnabled={false}
            style={styles.scrollview}
            refreshControl={
              <RefreshControl
              progressViewOffset={20}
              refreshing={this.state.isRefreshing || this.props.loading}
              onRefresh={this._onRefresh}
              tintColor="#1B3B79"
              title="Loading..."
              titleColor="#1B3B79"
              colors={['#1B3B79', '#1B3B79', '#1B3B79']}
              progressBackgroundColor="#1B3B79"
            />
          }>

          {
            groupedListKeys.length <= 0
            ?
            <Text style={{padding:20,textAlign:'center'}}>Your statistics is empty</Text>
            :
            groupedListKeys.map(this.renderItemsByMonth)
          }
          </ScrollView>
          </View>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    ...bindActionCreators(StatActions, dispatch),
    jumpTo: (i,tab) => dispatch(jumpTo(i,tab)),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  list: state.stat.list,
  loading: state.stat.loading,
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Home);
