export default function promiseMiddleware() {
	return next => action => {
		const { promise, type, ...rest } = action;

		if (!promise) return next(action);

		const SUCCESS = type + '_SUCCESS';
		const REQUEST = type + '_REQUEST';
		const FAILURE = type + '_ERROR';
		next({ ...rest, type: REQUEST });
		return promise
			.then(response => {
				if (response.error || (response.status && response.status == 'error')) {
					next({ ...rest, error:response, type: FAILURE });
					return response;
				}
				next({ ...rest, response, type: SUCCESS });
				return response;
			})
			.catch(error => {
				next({ ...rest, error, type: FAILURE });
				return error;
			});
	};
}
