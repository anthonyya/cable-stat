
import React, { Component } from 'react';
import CheckBox from '../CheckBox';
import { 
  View
} from 'react-native';
import Item from '../ListItems/Selectable';
import { Content, Button } from 'native-base';
import _ from 'lodash';
const quantity = [
2,3,4,5,6,7,8,9
];
class ListItem extends Component {
  constructor(props) {
    super(props);
  }
  onToggle = () =>{
    this.props.onToggle(this.props.item)
  }
  isSelectedQuantity = (q) =>{
    return !!(this.props.isChecked && this.props.isChecked.selectedQuantity == q);
  }
  onToggleQuantity = (q) =>{
    if (this.props.isChecked && this.props.isChecked.selectedQuantity == q) {
      this.props.onToggle({...this.props.item,selectedQuantity:q});
      return;
    }
    if (this.props.isChecked) {
      this.props.reset({...this.props.item,selectedQuantity:q});
    } else if (!this.props.isChecked) {
      this.props.onToggle({...this.props.item,selectedQuantity:q});
    }

  }
  render() {

    return (
      <Item
        topic={this.props.item.name.toUpperCase()}
        color={this.props.color}
        isChecked={this.props.isChecked && this.props.isChecked.selectedQuantity == 1}
        onToggle={this.onToggleQuantity.bind(this,1)}
      >
        {
          this.props.item.quantity
          &&
          quantity.map(itm=>{
            return <CheckBox text={itm} key={this.props.item.id+'_'+itm} onToggle={this.onToggleQuantity.bind(this,itm)} isChecked={this.isSelectedQuantity(itm)} color={this.props.color} />
          })
        }
        </Item>
    );
  }
}

export default ListItem
