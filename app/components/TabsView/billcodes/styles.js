
const React = require('react-native');

const { StyleSheet } = React;

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#ffffff',
  },
  content: {
    //backgroundColor: "white",
    padding: 10,
    flex: 1,
    zIndex:99,
    flexDirection: "row",
    alignItems: 'center'
  },
  description: {
    flex: 1
  },
  row: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  ic:{
    flex: 1,
    padding:10,
    alignSelf:'flex-start',
    alignItems: 'center'
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignItems: 'center',
  },
  list: {
    flex: 1,
  },
  contentContainer: {
    width: window.width
  },
  right: {
    flex: 1,
    alignItems: 'flex-end'
  },
  mt: {
    marginTop: 18,
  },
  leftSwipeItem: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 20
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 20
  },
  btnBlock:{
    padding:10,
  },
  bgw:{
    backgroundColor: '#FFF'
  }
});

