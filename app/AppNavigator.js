import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { BackAndroid, StatusBar, NavigationExperimental } from 'react-native';
import { connect } from 'react-redux';
import { Drawer } from 'native-base';
import { actions } from 'react-native-navigation-redux-helpers';
import { getCodes } from './actions/billcodes';

import NewStat from './components/newStat/index';
import NewBilcode from './components/TabsView/billcodes/newCode';

import TabsView from './components/TabsView/index';
import Login from './components/login/';
import Register from './components/register/';
import SplashPage from './components/splashscreen/';
import EditStat from './components/editstat';
import EditBillcode from './components/editbillcode';
import { statusBarColor } from './themes/base-theme';

const {
  popRoute,
  pushRoute
} = actions;

const {
  CardStack: NavigationCardStack,
} = NavigationExperimental;

async function getCache(key){
    try{
        let value = await AsyncStorage.getItem(key);
        return value.json();
    }
    catch(e){
        console.log('caught error', e);
    }
}

class AppNavigator extends Component {

  static propTypes = {
    drawerState: React.PropTypes.string,
    popRoute: React.PropTypes.func,
    closeDrawer: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
      routes: React.PropTypes.array,
    }),
  }

  componentDidMount() {
    AsyncStorage.getItem('@CurrentUser:me', (err, result) => {
      if (!result || err) {
        //this._renderScene({scene:{route:{key:}}})
      }
      console.log(result);
    });
    BackAndroid.addEventListener('hardwareBackPress', () => {
      const routes = this.props.navigation.routes;

      if (routes[routes.length - 1].key === 'home' || routes[routes.length - 1].key === 'login') {
        return false;
      }

      this.props.popRoute(this.props.navigation.key);
      return true;
    });
  }

  popRoute() {
    this.props.popRoute();
  }

  openDrawer() {
    this._drawer._root.open();
  }

  closeDrawer() {
    if (this.props.drawerState === 'opened') {
      this.props.closeDrawer();
    }
  }
  handleBackAction = (props,qw) =>{
    this.props.popRoute(this.props.navigation.key);
  }
  _renderScene(props,qw) { // eslint-disable-line class-methods-use-this
    switch (props.scene.route.key) {
      case 'splashscreen':
        return <SplashPage  />;
      case 'login':
        return <Login />;
      case 'home':
        return <TabsView />;
      case 'register':
        return <Register />
      case 'new':
        return <NewStat />
      case 'editstat':
        return <EditStat />
      case 'editbillcode':
        return <EditBillcode />
      case 'newbillcode':
        return <NewBilcode />
      default :
        return <SplashPage />;
    }
  }

  render() {
    return (
      <NavigationCardStack
        onNavigateBack={this.handleBackAction}
        navigationState={this.props.navigation}
        renderOverlay={this._renderOverlay}
        renderScene={this._renderScene}
      />
    );
  }
}

function bindAction(dispatch) {
  return {
    getCodes: () => dispatch(getCodes()),
    popRoute: key => dispatch(popRoute(key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(AppNavigator);