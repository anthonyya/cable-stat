
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

// 3rd party libraries
import Icon from 'react-native-vector-icons/MaterialIcons';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 15,
    height: 65,
    borderBottomColor: '#CCCCCC',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  deleteIcon: {
    flex: 1,
    alignSelf: 'center',
  },
  deleteText: {
    fontSize: 15,
    color: '#FC3D39',
    textAlign: 'left',
    marginTop: 20,
    marginBottom: 10,
    marginRight: 10,
  },
  stock: {
    flex: 8,
    flexDirection: 'column',
  },
  symbol: {
    flex: 1,
    flexDirection: 'row',
  },
  symbolText: {
    fontSize: 15,
    textAlign: 'left',
    marginTop: 10,
    marginBottom: 5,
    marginRight: 10,
  },
  marketText: {
    fontSize: 15,
    color: '#A6A6A6',
    textAlign: 'left',
    marginTop: 10,
    marginBottom: 5,
    marginRight: 10,
  },
  name: {
    flex: 1,
  },
  nameText: {
    fontSize: 10,
    textAlign: 'left',
    marginTop: 5,
    marginBottom: 5,
    marginRight: 10,
  },
  move: {
    flex: 1,
    color: '#A6A6A6',
    alignSelf: 'center',
  },
  moveText: {
    fontSize: 15,
    textAlign: 'left',
    marginTop: 20,
    marginBottom: 10,
    marginRight: 10,
  },
});

export default class StockCell extends React.Component {
  onPressDelete(symbol) {
  	if (condition) {
  		expression
  	}
    console.log('onPressDelete', symbol);
  }

  render() {
    return (
      <View style={styles.container}>
      	{
      		!!(this.props.onPressDelete)
      		&&
      		<Icon
      			onPress={this.props.onPressDelete}
	        	style={styles.deleteIcon}
	        	name="remove-circle"
	        	color="red"
	        	size={22}
	        />

      	}
        
        <View style={styles.stock}>
          <View style={styles.symbol}>
            <Text style={styles.symbolText}>
              {this.props.first}
            </Text>
            <Text style={styles.marketText}>
              {this.props.second}
            </Text>
          </View>
          <View style={styles.name}>
            <Text style={styles.nameText}>
              {this.props.mini}
            </Text>
          </View>
        </View>
        {
        	!this.props.hideMove
        	&&
        	<Icon style={styles.move} name="menu" color="white" size={22} />
        }
        
      </View>
    );
  }
}

StockCell.defaultProps = {

};