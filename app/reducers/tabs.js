import { tabReducer } from 'react-native-navigation-redux-helpers';

const tabs = {
  routes: [
    { key: 'stat', title: 'Items' },
    { key: 'settings', title: 'Settings' },
    { key: 'billcodes', title: 'Codes' }
  ],
  key: 'ApplicationTabs',
  index: 0
};

module.exports = tabReducer(tabs);