
const React = require('react-native');

const { StyleSheet } = React;

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
    backgroundColor: '#ffffff',
  },
  scrollview: {
    padding: 0,
  },
  bgw:{
  	backgroundColor: '#FFF'
  }
});

