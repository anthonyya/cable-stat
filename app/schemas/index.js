import { normalize, schema } from 'normalizr';
const bilcodeMergeStrategy = (entityA, entityB) => {
	return {
		...entityA,
		...entityB
	};
};
const bilcodeCategory = new schema.Entity('bilcodeCategories');
const bilcode = new schema.Entity('bilcodes', {
	category: bilcodeCategory
});