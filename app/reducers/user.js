import { AsyncStorage } from 'react-native';
import type { Action } from '../actions/types';
import { SET_USER } from '../actions/user';
import { ActionTypes } from '../constants';
import api from '../api';
async function setCache(key,val){
    try{
        let value = await AsyncStorage.setItem(key,val);
        return value;
    }
    catch(e){
        console.log('caught error', e);
    }
}

export type State = {
    name: string,
    token: string
}

const initialState = {
  name: '',
  token: ''
};

export default function (state:State = initialState, action:Action): State {
	switch (action.type) {
		case ActionTypes.SET_USER_SUCCESS:{
			if (action.response.info) {
				api.setAuth(action.response.info.id);
				setCache('@CurrentUser:me',JSON.stringify(action.response.info));
				return {
			      ...state,
			      name: 'Blanc',
			      token: action.response.info.id,
			    };
			} else {
				return state;
			}
			
		}
		case ActionTypes.UNSET_USER:{
			AsyncStorage.removeItem('@CurrentUser:me');
			AsyncStorage.removeItem('@Stat:all');
			AsyncStorage.removeItem('@BillCodes:all');
			AsyncStorage.removeItem('@RemoteBillCodes:all');
			
			return initialState;
		}
		default: {
			return state;
		}
	}
}
