
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {View,Text,TextInput,ScrollView,} from 'react-native';
import { actions } from 'react-native-navigation-redux-helpers';
import Spinner from 'react-native-loading-spinner-overlay';
import {
  Container,
  Left, 
  Right, 
  Title, 
  Content, 
  Button, 
  Icon,
  Form,
  Body,
  Item, 
  Label, 
  Input,
  Switch,
  ListItem,
  List,
  Separator,
} from 'native-base';
import BTN from '../Button';
import * as BillcodeActions from '../../actions/billcodes';

import styles from './style';
import Header from '../Header';
import { bindActionCreators } from 'redux';
import { createSelector } from 'reselect';
const {
  reset,
  popRoute,
} = actions;

class EditBillcodePage extends Component {
  constructor(props) {
    super(props);
    this.state = {...props.bill}
  }
  popRoute = () => {
    this.props.popRoute(this.props.navigation.key);
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps.stat);
  }

  onSave = () =>{
    this.setState({
      loading:true
    })
    this.props.updateBillCode(this.state).then(()=>{
      this.setState({
        loading:false
      })
      this.props.getBillCodes()
      this.props.popRoute(this.props.navigation.key)
    }).catch(()=>{
      this.setState({
        loading:false
      })
      this.props.popRoute(this.props.navigation.key)
    })
  }
  render() {
    return (
      <Container>
                
      
        <Header
          title="Edit"
          foreground="dark"
          leftItem={{
            title: 'Back',
            onPress: this.props.popRoute.bind(this,this.props.navigation.key),
          }}
          rightItem={{
            title: 'Save',
            onPress: this.onSave,
          }}
        />
        <Content >
          <Separator bordered noTopBorder />
          <View style={styles.bgw}>
          <Form>
              <Item stackedLabel last>
                  <Label>Name</Label>
                  <Input
                    placeholder="Name"
                    value={this.state.name}
                    onChangeText={(name) => this.setState({ name })}
                  />
              </Item>
              <Item stackedLabel last>
                  <Label>Price</Label>
                  <Input
                    placeholder="Price"
                    value={this.state.pricePerOne}
                    onChangeText={(pricePerOne) => this.setState({ pricePerOne })}
                  />
              </Item>
          </Form>
          <Separator bordered noTopBorder />
          <ListItem icon >
              <Left>
                <Button style={{ backgroundColor: '#FF9501' }}>
                  <Icon active name="md-checkmark-circle-outline" />
                </Button>
              </Left>
              <Body>
                <Text>Quantity changer</Text>
              </Body>
              <Right>
                  <Switch onValueChange={(quantity)=>this.setState({quantity})} value={this.state.quantity} />
              </Right>
          </ListItem>
          </View>
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    ...bindActionCreators(BillcodeActions,dispatch),
    popRoute: key => dispatch(popRoute(key))
  };
}
const mapStateToProps = createSelector(
   (state)=>state.cardNavigation,
   (state)=>state.billcodes.editing,
   (navigation,bill)=>{
    return {
      navigation,bill
    }
   }
);
export default connect(mapStateToProps, bindAction)(EditBillcodePage);
